package password;

import java.util.Random;

/** 
 * 
 * @author ramses Trejo
 * 
 * Validates passwords and is being developed using TDD
 *Used regex for digit validation

 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	/**
	 * 
	 * @param password
	 * @return true if the number of characters is 8 or more. No spaces are allowed.
	 * Opted for a single return rather than 2 separate returns.
	 */
	
	public static boolean isValidLength( String password ) {
		
		return password.indexOf( " " ) < 0 && password.length( ) >= MIN_LENGTH;
	}
	
	public static boolean hasValidDigitCount( String password ) {
		return password != null && password.matches( "([^0-9]*[0-9]){2}.*" );  
	}
	
	
	protected static String generateRandomPassword( ) {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder( );
        Random rnd = new Random();
        int size = (int) ( rnd.nextFloat( ) * 16 );
        while (salt.length() < size ) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }	
	
	public static void main( String args [ ] ) {
		System.out.println( "Password Validator 1.0" );
		
		String password = generateRandomPassword( );
		
		System.out.println( "Password generated for testing " + password );
		System.out.println( "Is valid password ? : " + ( isValidLength( password ) && hasValidDigitCount( password ) ) );
		
	}	
	

}
